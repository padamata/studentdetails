/*
 *  				###... Student Details View ...###
 * */
var StudentDetailsView = Backbone.View.extend({

	tagName : 'div',
	id : 'header',

	initialize : function() {
		this.template = _.template(temp.get("header"));

		this.model.bind("change", this.render, this);
		this.model.bind("destroy", this.close, this);
	},
	
	render : function() {
		$(this.el).html(this.template(this.model.toJSON()));
		return this;
	},
	
	events: {
		"click .new" : "newStudent"
    },

    newStudent : function(event) {
    	console.log("new student");
		app.navigate("Student/new", true);
		return false;
	},

});

/*
 * 					###... Student List View ...###
 * */
var StudentListView = Backbone.View.extend({

	tagName : "ul",

	initialize : function() {
		this.model.bind("reset", this.render, this);
		this.model.bind("add", this.render, this);
		this.model.bind("change", this.render, this);
	},

	render : function() {
		_.each(this.model.models, function(StudentData) {
			$(this.el).append(new StudentListItemView({
				model : StudentData
			}).render().el);
		}, this);
		return this;
	}

});

/*
 *					 ###... Student List ITEM View ...###
 */
var StudentListItemView = Backbone.View.extend({

	tagName : "li",

	initialize : function() {
		this.template = _.template(temp.get('student-list-item'));
		this.model.bind("change", this.render, this);
		this.model.bind("destroy", this.close, this);
	},

	render : function(eventName) {
		$(this.el).html(this.template(this.model.toJSON()));
		return this;
	},

});

/*
 *					 ###... Student View ...###
 */
var StudentView = Backbone.View.extend({
	
	tagName : 'div',
	//id : 'content',

	initialize : function() {
		this.template = _.template(temp.get("student-details"));
		this.model.bind("change", this.render, this);
		
	},

	render : function(eventName) {
		$(this.el).html(this.template(this.model.toJSON()));
		var _this = this;
		setTimeout(function(){
			_this.afterRender(_this);
		}, 0);
		return this;
	},
	
	afterRender : function(_this){
		this.studentMarksListView = new StudentMarksListView({
			model: _this.model.get("studentmarkslist")
		});
		$("#marksTable").append(this.studentMarksListView.render().el);
  	},

	events : {
		"change input" : "change",
		"click #addRow" : "addRow",
		"click .save" : "saveStudent",
		"click .delete" : "deleteStudent"
	},
	
//	###... Adding a row when clicked ...###
  	addRow : function(){
  		studentMarksList = new StudentMarksList();
  		this.model.get("studentmarkslist").add(studentMarksList);
  	},

	change : function(event) {
		var target = event.target;
		console.log('changing ' + target.id + ' from: ' + target.defaultValue + ' to: ' + target.value);
	},

	saveStudent : function(e) {
		console.log("save .....:::");
		e.preventDefault();
		this.$('.alert').hide();
		

		var data = this.$('form').serializeObject();
		if (this.model.set(data)) {
			this.$('.alert-success').fadeIn();
			if (this.model.isNew()) {
				var self = this;
				app.studentList.create(this.model, {
					success : function() {
						app.navigate('Student/' + self.model.id, false);
					}
				});
			} else {
				this.model.save();
			}

		} else {
			this.$('.alert-error').fadeIn();
		}
		return false;
	},

	deleteStudent : function() {
		this.model.destroy({
			success : function() {
				alert('Student deleted successfully');
				window.history.back();
			}
		});
		return false;
	},

	close : function() {
		$(this.el).unbind();
		$(this.el).empty();
	}
});
/*
 * 			###... Student Marks List Item View ...###
 * */
var StudentMarksListView = Backbone.View.extend({

	initialize: function() {
	  this.model.bind("add", this.render, this);
	  this.model.bind("change", this.render, this);
	},
	
	tagName: 'tbody',
	id: 'table-body',
	className: 'tab',
	
	render: function() {
		$(this.el).empty();
		_.each(this.model.models,function(student){
			this.studentMarksListItemView = new StudentMarksListItemView({
				model: student
			});
			console.log(this.studentMarksListItemView.render().el);
			$(this.el).append(this.studentMarksListItemView.render().el);
		},this);
		
		return this;	
	}

});
/*
 * 			###... Student Marks List Item View ...###
 * */
var StudentMarksListItemView = Backbone.View.extend({

	initialize: function() {
		_.bindAll(this, "render");
		this.template = _.template(temp.get("studentMarksList"));
		 this.model.bind("change", this.render, this);
	},
	
	tagName: 'tr',
	className:'item-row',
	
	render: function() {
		$(this.el).html(this.template(this.model.toJSON()));
		return this;
	}
});

