/*
 * 			 ###... Router ...###
 * */
var AppRouter = Backbone.Router.extend({
/*
 * 			###... Header view ...###
 * */
	initialize : function() {
		console.log("starting up");
		this.studentModel = new Student();
		this.studentDetailsView = new StudentDetailsView({
			model : this.studentModel
		});
		$('#header').html(this.studentDetailsView.render().el);
	},

	routes : {
		"" : "student",
		"Student/new" : "newStudent",
		"Student/:id" : "studentDetails",
		
	},
/*
 * 		  ###... Side-bar View ...###
 * */
	student : function() {
		console.log("starting up 2");
		this.studentList = new StudentCollection();
		this.studentList.fetch({ 
			success : function(data){
				//console.log(data);
			}});
		this.studentListView = new StudentListView({
			model : this.studentList
		});
		$('#sidebar').html(this.studentListView.render().el);
	},

/*
 * 			###... Student Details View ...###
 * */
	studentDetails : function(id) {
		console.log("starting up 3");
		this.student = app.studentList.get(id);
		this.studentView = new StudentView({
			model : this.student
		});
		$('#content').html(this.studentView.render().el);
	},

	newStudent : function() {
		console.log("starting up 4");
		app.showView('#content', new StudentView({model : new Student()}));
	},

	showView: function(selector, view) {
        if (this.currentView)
            this.currentView.close();
        $(selector).html(view.render().el);
        this.currentView = view;
        return view;
    },
    
	studentSave : function() { 
		this.studentView = new StudentView({
			model : this.student
		});
		$('#saveArea').html(this.studentView.render().el);
	},

});

temp.loadTemplates([ 'header', 'student-details', 'student-list-item', 'studentMarksList' ],
function() {
	console.info("helllllllo");
	app = new AppRouter();
	Backbone.history.start();
});