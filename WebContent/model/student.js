/*
 * 				###...Invoice Model...###
 * */
var Student = Backbone.Model.extend({
	urlRoot : "student",
	defaults : {
		"id" : "",
		"firstname" : "",
		"lastname" : "",
		"syoj" : "",
		"syop" : "",
		"course" : "",
	    "studentmarkslist": null
	},
	
	set: function(attributes, options) { 
		if(attributes.studentmarkslist!==undefined && attributes.studentmarkslist!=="undefined") {
	        attributes.studentmarkslist = new StudentMarksList(attributes.studentmarkslist);
    	}
		return Backbone.Model.prototype.set.call(this, attributes, options);
    }

});

/*
 * 				###... Student Collection ...###
 * */
var StudentCollection = Backbone.Collection.extend({
	model : Student,
	url : "StudentData"
});

/*
 * 				###... Student Marks List ...###
 * */
var StudentMarksListItem = Backbone.Model.extend({
	defaults : {
		"subject1" : "",
		"subject2" : "",
		"subject3" : "",
		"subject4" : "",
		"subject5" : "",
		"subject6" : "",
		"yearsem" : "",
		"sid" : "",
	},
	
});

var StudentMarksList = Backbone.Collection.extend({
	model: StudentMarksListItem
});


//var studentList = new StudentCollection();
